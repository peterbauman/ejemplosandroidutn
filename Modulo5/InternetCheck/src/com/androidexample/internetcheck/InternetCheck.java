package com.androidexample.internetcheck;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class InternetCheck extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final Button Save = (Button) findViewById(R.id.save);
        
        Save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				isInternetOn();
			}
        });	
    }
    
    public final boolean isInternetOn() {
    	
    	// get Connectivity Manager object to check connection
		ConnectivityManager connec =  (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);
		
		   // Check for network connections
			if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
			     connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
			     connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
			     connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
			   
				// if connected with internet
				
			    Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
			    return true;
			    
			} else if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
			  
				Toast.makeText(this, " Not Connected ", Toast.LENGTH_LONG).show();
			    return false;
			}
		  return false;
		}
}