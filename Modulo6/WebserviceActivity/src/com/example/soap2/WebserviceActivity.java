package com.example.soap2;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WebserviceActivity extends Activity {

	private final String NAMESPACE = "http://www.w3schools.com/webservices/";
	private final String URL = "http://www.w3schools.com/webservices/tempconvert.asmx";
	private final String SOAP_ACTION = "http://www.w3schools.com/webservices/CelsiusToFahrenheit";
	private final String METHOD_NAME = "CelsiusToFahrenheit";

            Button b;

            TextView tv;

            EditText et;

            public void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.main);

                        et = (EditText) findViewById(R.id.editText1);

                        tv = (TextView) findViewById(R.id.tv_result);

                        b = (Button) findViewById(R.id.button1);

                        b.setOnClickListener(new OnClickListener() {
                                    public void onClick(View v) {

                                                String result = getFahrenheit(et.getText().toString());

                                                tv.setText(result+"� F");
                                    }
                        });

            }

            public String getFahrenheit(String celsius) {

                        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

                        PropertyInfo celsiusPI = new PropertyInfo();
                        celsiusPI.setName("Celsius");
                        celsiusPI.setValue(celsius);
                        celsiusPI.setType(double.class);
                        request.addProperty(celsiusPI);

                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                                                SoapEnvelope.VER11);
                        envelope.dotNet = true;
                        envelope.setOutputSoapObject(request);
                        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

                        try {
                                    androidHttpTransport.call(SOAP_ACTION, envelope);
                                    SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                                    Log.i("Webservice Output", response.toString());

                                    return response.toString();

                        } catch (Exception e) {
                                    e.printStackTrace();
                        }

                        return null;
            }
}
