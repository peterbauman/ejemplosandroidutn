package com.app.locator;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends android.support.v4.app.FragmentActivity
		implements GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private GoogleMap mapa;

	ArrayList<MarkerOptions> pointList = new ArrayList<MarkerOptions>();

	private Location currentLocation;
	private MarkerOptions mylocation;
	private double currentLatitude, currentLongitude;

	private LocationClient mLocationClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		mylocation = new MarkerOptions();
		mapa = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (errorCode != ConnectionResult.SUCCESS)
		{
		        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, 
		                                1, new DialogInterface.OnCancelListener() {
											
											@Override
											public void onCancel(DialogInterface dialog) {
												// TODO Auto-generated method stub
												
											}
										});
		        errorDialog.show();
		}
		
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		    }
		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}
		  };

		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		
		mLocationClient = new LocationClient(this, this, this);

		// controla y agrega puntos que se hayan creado anteriormente
		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("points")) {
				pointList = savedInstanceState.getParcelableArrayList("points");
				if (pointList != null) {
					for (int i = 0; i < pointList.size(); i++) {
						mapa.addMarker(pointList.get(i));
					}
				}
			}
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		if (mLocationClient != null) {
			mLocationClient.connect();
		}
	}

	@Override
	protected void onStop() {
		mLocationClient.disconnect();
		super.onStop();

	}

	private void sendLocation() {
		JSONParser jsonParser = new JSONParser();
		String loginUrl = "http://eserver.site40.net/register.php";
		String deviceId = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", deviceId));
		params.add(new BasicNameValuePair("latitude", Double
				.toString(currentLatitude)));
		params.add(new BasicNameValuePair("longitude", Double
				.toString(currentLongitude)));
		Log.d("request!", "starting");

		// Posting user data to script
		jsonParser.makeHttpRequest(loginUrl, "POST", params);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// en el caso de rotar la pantalla o apretar el boton Home
		// se guardan los puntos marcados hasta el momento

		outState.putParcelableArrayList("points", pointList);
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getTitle().toString().equals("Limpiar Mapa")) {
			mapa.clear();
			pointList.clear();
		} else if (item.getTitle().toString().equals("Cambiar Mapa")) {

			cambiarTipoMapa();
		}
		return false;

	}

	public void findLocation() {

		currentLocation = mLocationClient.getLastLocation();
		Log.d("location", currentLocation.toString());
		updateLocation(currentLocation);
		LatLng coord = new LatLng(currentLatitude, currentLongitude);
		mylocation.position(coord).title("My Location");

		agregarMarcador(mylocation);

	}

	void updateLocation(Location location) {
		//currentLocation = location;
		currentLatitude = location.getLatitude();
		currentLongitude = location.getLongitude();
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public void getNearbyPoints() {
		// verifica conexion a internet
		if (isOnline()) {
			String urlLocations = "http://eserver.site40.net/web-service.php?lat="
					+ currentLatitude
					+ "&long="
					+ currentLongitude
					+ "&get=locations";
			String urlUsers = "http://eserver.site40.net/web-service.php?lat="
					+ currentLatitude + "&long=" + currentLongitude
					+ "&get=users";

			String[] send = { urlLocations, urlUsers };
			// String url =
			// "http://eserver.site40.net/web-service.php?lat=-34.5309&long=-58.5201&get=locations";
			new MyAsyncTask().execute(send);

		} else {

			Toast.makeText(this, "No se encuentra conectado a Internet",
					Toast.LENGTH_LONG).show();
		}

	}

	public void limpiarMapa() {
		mapa.clear();

	}

	public void cambiarTipoMapa() {

		final String items[] = { "Normal", "Satelite", "Terreno", "Hibrido" };
		AlertDialog.Builder ab = new AlertDialog.Builder(this);
		ab.setTitle("Dialog Title");
		ab.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface d, int choice) {
				if (choice == 0) {

					mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				} else if (choice == 1) {
					mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

				} else if (choice == 2) {

					mapa.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
				} else if (choice == 3) {

					mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);

				}

			}
		});
		ab.show();

		// mapa.setMapType();

	}

	public void agregarMarcador(MarkerOptions opciones) {

		pointList.add(opciones);
		mapa.addMarker(opciones);

		LatLngBounds.Builder b = new LatLngBounds.Builder();

		b.include(opciones.getPosition());

		CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(
				opciones.getPosition(), 14);

		mapa.animateCamera(cu);

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			/*
			 * If no resolution is available, display a dialog to the user with
			 * the error.
			 */
			Toast.makeText(this,
					"No resolution, Error:" + connectionResult.getErrorCode(),
					Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();

			// una vez conectado a Google Services busca la ubicacion y consigue
			// puntos cercanos
			findLocation();
			getNearbyPoints();

	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "Conexion Perdida",
				Toast.LENGTH_SHORT).show();

	}

	private class MyAsyncTask extends AsyncTask<String, String, String> {
		ArrayList<MarkerOptions> locations = null;
		ArrayList<MarkerOptions> users = null;

		@Override
		protected String doInBackground(String... args) {
			// obtiene todas las ubicaciones cercanas

			locations = LocationParser.getData(args[0]);
			users = LocationParser.getData(args[1]);

			// envia al web service la ubicacion del dispositivo
			sendLocation();
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			for (int i = 0; i < locations.size(); i++) {
				agregarMarcador(locations.get(i));
			}
			for (int i = 0; i < users.size(); i++) {
				agregarMarcador(users
						.get(i)
						.icon(BitmapDescriptorFactory
								.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
			}

		}

	}

	// Define a DialogFragment that displays the error dialog
	public static class ErrorDialogFragment extends DialogFragment {
		// Global field to contain the error dialog
		private Dialog mDialog;

		// Default constructor. Sets the dialog field to null
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		// Set the dialog to display
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		// Return a Dialog to the DialogFragment.
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	/*
	 * Handle results returned to the FragmentActivity by Google Play services
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Decide what to do based on the original request code
		switch (requestCode) {
		case CONNECTION_FAILURE_RESOLUTION_REQUEST:
			/*
			 * If the result code is Activity.RESULT_OK, try to connect again
			 */
			switch (resultCode) {
			case Activity.RESULT_OK:
				/*
				 * Try the request again
				 */
				break;
			}
		}
	}

}
