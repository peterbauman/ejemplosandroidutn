package com.app.locator;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationParser {

	// Se conecta a
	// "http://eserver.site40.net/web-service.php?lat=-34.5309&long=-58.5201&get=locations";
	// , y parsea el Json
	// Resultante

	public static ArrayList<MarkerOptions> getData(String url) {

		ArrayList<MarkerOptions> nearPoints = new ArrayList<MarkerOptions>();
		String latitude = "";
		String longitude = "";
		String title = "";
		JSONParser jParser = new JSONParser();
		JSONArray jsonArray = jParser.getJSONFromUrl(url);

		try {

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject explrObject = jsonArray.getJSONObject(i);
				latitude = explrObject.getString("latitude");
				longitude = explrObject.getString("longitude");
				title = explrObject.getString("name");

				nearPoints.add(new MarkerOptions().position(
						new LatLng(Double.parseDouble(latitude), Double
								.parseDouble(longitude))).title(title));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nearPoints;
	}

}
