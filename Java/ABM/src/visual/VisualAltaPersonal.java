package visual;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import Clases.Administrativo;
import Clases.Vendedor;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.awt.Label;
import java.awt.Font;
import java.awt.TextField;
import java.awt.Choice;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class VisualAltaPersonal extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Dao<Administrativo, String> accountDao;
	Dao<Vendedor, String> vendedorDao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VisualAltaPersonal dialog = new VisualAltaPersonal();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VisualAltaPersonal() {
		setTitle("Alta de Personal");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

		Label label = new Label("Alta de Personal");
		Label label_1 = new Label("Sector:");
		Label label_2 = new Label("Nombre");
		Label label_3 = new Label("Apellido");
		Label label_4 = new Label("Edad");
		Label label_5 = new Label("Zona");
		TextField nombreTxt = new TextField();
		TextField apellidoTxt = new TextField();
		TextField edadTxt = new TextField();
		Choice sectorChoice = new Choice();
		Choice zonaChoice = new Choice();
		Button altaButton = new Button("Dar de Alta");
		altaButton.setEnabled(false);
		sectorChoice.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (sectorChoice.getSelectedIndex() == 0) {
					zonaChoice.setEnabled(false);
					altaButton.setEnabled(false);
				}
				if (sectorChoice.getSelectedIndex() == 1) {
					zonaChoice.setEnabled(false);
					altaButton.setEnabled(true);

				}
				if (sectorChoice.getSelectedIndex() == 2) {
					zonaChoice.setEnabled(true);
					altaButton.setEnabled(true);

				}
			}
		});

		label.setAlignment(Label.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		label.setBounds(10, 10, 414, 22);
		getContentPane().add(label);

		label_1.setBounds(20, 38, 62, 22);
		getContentPane().add(label_1);

		label_2.setBounds(20, 69, 62, 22);
		getContentPane().add(label_2);

		label_3.setBounds(20, 97, 62, 22);
		getContentPane().add(label_3);

		label_4.setBounds(20, 125, 62, 22);
		getContentPane().add(label_4);

		label_5.setBounds(20, 151, 62, 22);
		getContentPane().add(label_5);

		nombreTxt.setBounds(88, 69, 151, 22);
		getContentPane().add(nombreTxt);

		apellidoTxt.setBounds(88, 97, 151, 22);
		getContentPane().add(apellidoTxt);

		edadTxt.setBounds(88, 125, 54, 22);
		getContentPane().add(edadTxt);

		sectorChoice.setBounds(88, 40, 151, 20);
		sectorChoice.addItem("Seleccione...");
		sectorChoice.addItem("Administracion");
		sectorChoice.addItem("Ventas");
		getContentPane().add(sectorChoice);

		zonaChoice.setEnabled(false);
		zonaChoice.setBounds(88, 151, 151, 20);
		zonaChoice.addItem("Norte");
		zonaChoice.addItem("Sur");
		zonaChoice.addItem("Este");
		zonaChoice.addItem("Oeste");
		zonaChoice.addItem("Centro");
		getContentPane().add(zonaChoice);

		altaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// String con la conexi�n del puente a la base de datos. Si no
				// est� previamente creada nos dar� error.
				String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
				ConnectionSource connectionSource;
				try {
					connectionSource = new JdbcConnectionSource(databaseUrl);
					((JdbcConnectionSource) connectionSource)
							.setUsername("root");
					((JdbcConnectionSource) connectionSource).setPassword("");

					if (sectorChoice.getSelectedIndex() == 1) {
						if (nombreTxt.getText() != ""
								&& apellidoTxt.getText() != ""
								&& edadTxt.getText() != "") {

							accountDao = DaoManager.createDao(connectionSource,
									Administrativo.class);

							// create table
							TableUtils.createTableIfNotExists(connectionSource,
									Administrativo.class);
							Administrativo cp = new Administrativo();

							cp.setNombre(nombreTxt.getText());
							cp.setApellido(apellidoTxt.getText());
							cp.setEdad(Integer.parseInt(edadTxt.getText()));
							cp.setSector("Administracion");

							accountDao.create(cp);
							JOptionPane.showMessageDialog(null,
									"Se ha agregado un Administrativo n�mero "
											+ cp.getId());
							nombreTxt.setText("");
							apellidoTxt.setText("");
							edadTxt.setText("");
							sectorChoice.select(0);
							zonaChoice.select(0);

						}
					}
					if (sectorChoice.getSelectedIndex() == 2) {
						if (nombreTxt.getText() != ""
								&& apellidoTxt.getText() != ""
								&& edadTxt.getText() != "") {

							vendedorDao = DaoManager.createDao(
									connectionSource, Vendedor.class);

							// create table
							TableUtils.createTableIfNotExists(connectionSource,
									Vendedor.class);
							Vendedor cp = new Vendedor();

							cp.setNombre(nombreTxt.getText());
							cp.setApellido(apellidoTxt.getText());
							cp.setEdad(Integer.parseInt(edadTxt.getText()));
							cp.setZona(zonaChoice.getSelectedItem());

							vendedorDao.create(cp);
							JOptionPane.showMessageDialog(
									null,
									"Se ha agregado un Vendedor n�mero "
											+ cp.getId());
							nombreTxt.setText("");
							apellidoTxt.setText("");
							edadTxt.setText("");
							sectorChoice.select(0);
							zonaChoice.select(0);
							zonaChoice.setEnabled(false);

						}
					}

				} catch (SQLException e1) {
					e1.printStackTrace();
				}

			}
		});
		altaButton.setBounds(215, 208, 70, 22);
		getContentPane().add(altaButton);

		Button cancelarButton = new Button("Cancelar");
		cancelarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VisualAltaPersonal.this.dispose();
			}
		});
		cancelarButton.setBounds(313, 208, 70, 22);
		getContentPane().add(cancelarButton);

	}
}
