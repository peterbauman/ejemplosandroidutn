package visual;

import java.awt.Button;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VisualInicio extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisualInicio frame = new VisualInicio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VisualInicio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 311, 263);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Button button = new Button("Alta de Personal");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VisualAltaPersonal ventana = new VisualAltaPersonal();
				ventana.setVisible(true);
			}
		});
		button.setFont(new Font("Dialog", Font.BOLD, 15));
		button.setBounds(57, 45, 197, 32);
		contentPane.add(button);
		
		Button button_1 = new Button("Baja - Modificacion");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VisualModiPersonal vent = new VisualModiPersonal();
				vent.setVisible(true);
			}
		});
		button_1.setFont(new Font("Dialog", Font.BOLD, 15));
		button_1.setBounds(57, 102, 197, 32);
		contentPane.add(button_1);
		
		Button button_2 = new Button("Salir");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VisualInicio.this.dispose();
			}
		});
		button_2.setFont(new Font("Dialog", Font.BOLD, 15));
		button_2.setBounds(174, 165, 111, 32);
		contentPane.add(button_2);
	}

}
