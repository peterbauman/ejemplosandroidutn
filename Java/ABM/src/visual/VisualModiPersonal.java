package visual;

import java.awt.Button;
import java.awt.Choice;
import java.awt.EventQueue;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import Clases.Administrativo;
import Clases.Vendedor;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public class VisualModiPersonal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VisualModiPersonal frame = new VisualModiPersonal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VisualModiPersonal() {
		setTitle("Modificar Datos del Personal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 679, 268);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		TextField tBuscar = new TextField();
		TextField tID = new TextField();
		TextField tNombre = new TextField();
		TextField tApellido = new TextField();
		TextField tEdad = new TextField();
		Button bGuardar = new Button("GUARDAR");
		Label label_1 = new Label("ID");
		Label label_2 = new Label("Nombre");
		Label label_3 = new Label("Apellido");
		JRadioButton rVendedor = new JRadioButton("ID Vendedor");
		Label label_4 = new Label("Edad");
		JRadioButton rAdministrativo = new JRadioButton("ID Administrativo");
		Label label = new Label("Buscar por:");
		Choice zonaSelect = new Choice();
		Button bBuscar = new Button("BUSCAR");
		Button bModificar = new Button("MODIFICAR");
		Button bEliminar = new Button("ELIMINAR");

		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rAdministrativo.isSelected()) {
					Dao<Administrativo, Integer> admDel;
					Administrativo aDel = new Administrativo();

					aDel.setId(Integer.parseInt(tID.getText()));

					String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
					ConnectionSource connectionSource;
					try {
						connectionSource = new JdbcConnectionSource(databaseUrl);
						((JdbcConnectionSource) connectionSource)
								.setUsername("root");
						((JdbcConnectionSource) connectionSource)
								.setPassword("");
						admDel = DaoManager.createDao(connectionSource,
								Administrativo.class);

						admDel.deleteById(aDel.getId());

						JOptionPane.showMessageDialog(null,
								"Registro Eliminado correctamente");

						bModificar.enable(false);
						bGuardar.enable(false);
						bEliminar.enable(false);

						tBuscar.setText("");
						tID.setText("");
						tNombre.setText("");
						tNombre.enable(false);
						tApellido.setText("");
						tApellido.enable(false);
						tEdad.setText("");
						tEdad.enable(false);
						zonaSelect.select(0);
						zonaSelect.enable(false);

					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				} else {
					Dao<Vendedor, Integer> venDel;
					Vendedor vDel = new Vendedor();

					vDel.setId(Integer.parseInt(tID.getText()));

					String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
					ConnectionSource connectionSource;
					try {
						connectionSource = new JdbcConnectionSource(databaseUrl);
						((JdbcConnectionSource) connectionSource)
								.setUsername("root");
						((JdbcConnectionSource) connectionSource)
								.setPassword("");
						venDel = DaoManager.createDao(connectionSource,
								Vendedor.class);

						venDel.deleteById(vDel.getId());

						JOptionPane.showMessageDialog(null,
								"Registro Eliminado correctamente");

						bModificar.enable(false);
						bGuardar.enable(false);
						bEliminar.enable(false);

						tBuscar.setText("");
						tID.setText("");
						tNombre.setText("");
						tNombre.enable(false);
						tApellido.setText("");
						tApellido.enable(false);
						tEdad.setText("");
						tEdad.enable(false);
						zonaSelect.select(0);
						zonaSelect.enable(false);

					} catch (SQLException e3) {
						e3.printStackTrace();
					}

				}

			}
		});

		bGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rAdministrativo.isSelected()) {
					Dao<Administrativo, String> admup;
					Administrativo aUP = new Administrativo();
					aUP.setSector("Administrativo");
					aUP.setNombre(tNombre.getText());
					aUP.setApellido(tApellido.getText());
					aUP.setEdad(Integer.parseInt(tEdad.getText()));
					aUP.setId(Integer.parseInt(tID.getText()));

					String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
					ConnectionSource connectionSource;
					try {
						connectionSource = new JdbcConnectionSource(databaseUrl);
						((JdbcConnectionSource) connectionSource)
								.setUsername("root");
						((JdbcConnectionSource) connectionSource)
								.setPassword("");
						admup = DaoManager.createDao(connectionSource,
								Administrativo.class);

						admup.update(aUP);
						JOptionPane.showMessageDialog(null,
								"Registro Actualizado correctamente");

						bModificar.enable(false);
						bGuardar.enable(false);
						bEliminar.enable(false);

						tBuscar.setText("");
						tID.setText("");
						tNombre.setText("");
						tNombre.enable(false);
						tApellido.setText("");
						tApellido.enable(false);
						tEdad.setText("");
						tEdad.enable(false);
						zonaSelect.select(0);
						zonaSelect.enable(false);

					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				} else {

					Dao<Vendedor, String> venup;
					Vendedor vUP = new Vendedor();
					vUP.setZona(zonaSelect.getSelectedItem());
					;
					vUP.setNombre(tNombre.getText());
					vUP.setApellido(tApellido.getText());
					vUP.setEdad(Integer.parseInt(tEdad.getText()));
					vUP.setId(Integer.parseInt(tID.getText()));

					String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
					ConnectionSource connectionSource;
					try {
						connectionSource = new JdbcConnectionSource(databaseUrl);
						((JdbcConnectionSource) connectionSource)
								.setUsername("root");
						((JdbcConnectionSource) connectionSource)
								.setPassword("");
						venup = DaoManager.createDao(connectionSource,
								Vendedor.class);

						venup.update(vUP);
						JOptionPane.showMessageDialog(null,
								"Registro Actualizado correctamente");

						bModificar.enable(false);
						bGuardar.enable(false);
						bEliminar.enable(false);

						tBuscar.setText("");
						tID.setText("");
						tNombre.setText("");
						tNombre.enable(false);
						tApellido.setText("");
						tApellido.enable(false);
						tEdad.setText("");
						tEdad.enable(false);
						zonaSelect.select(0);
						zonaSelect.enable(false);

					} catch (SQLException e3) {
						e3.printStackTrace();
					}

				}
			}
		});

		bModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// zonaSelect.enable(false);
				if (rAdministrativo.isSelected()) {

					bModificar.enable(false);
					bGuardar.enable(true);
					tNombre.enable(true);
					tApellido.enable(true);
					tEdad.enable(true);

					bEliminar.enable(false);
					
					
				} else {
					bModificar.enable(false);
					bGuardar.enable(true);
					tNombre.enable(true);
					tApellido.enable(true);
					tEdad.enable(true);
					zonaSelect.enable(true);

					bEliminar.enable(false);
				}
			}
		});

		tBuscar.setBounds(106, 49, 155, 22);
		contentPane.add(tBuscar);

		bBuscar.addActionListener(new ActionListener() {
			private Dao<Administrativo, Integer> admbus;
			private Dao<Vendedor, Integer> venBus;

			public void actionPerformed(ActionEvent arg0) {
				if (rVendedor.isSelected() || rAdministrativo.isSelected()) {

					bModificar.enable(false);
					bEliminar.enable(false);
					bGuardar.enable(false);

					tID.setText("");
					tNombre.setText("");
					tNombre.enable(false);
					tApellido.setText("");
					tApellido.enable(false);
					tEdad.setText("");
					tEdad.enable(false);
					zonaSelect.select(0);
					zonaSelect.enable(false);

					if (rVendedor.isSelected()) {
						if (!tBuscar.getText().equals("")) {

							String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
							ConnectionSource connectionSource;
							try {
								connectionSource = new JdbcConnectionSource(
										databaseUrl);
								((JdbcConnectionSource) connectionSource)
										.setUsername("root");
								((JdbcConnectionSource) connectionSource)
										.setPassword("");

								venBus = DaoManager.createDao(connectionSource,
										Vendedor.class);

								if (venBus.queryForId(Integer.parseInt(tBuscar
										.getText())) != null) {
									Vendedor resul = venBus.queryForId(Integer
											.parseInt(tBuscar.getText()));
									tID.setText(Integer.toString(resul.getId()));
									tNombre.setText(resul.getNombre());
									tApellido.setText(resul.getApellido());
									tEdad.setText(Integer.toString(resul
											.getEdad()));
									zonaSelect.select(resul.getZona());

									bEliminar.enable(true);
									bModificar.enable(true);
								} else {
									JOptionPane.showMessageDialog(null,
											"no se encontraron registros");
									bModificar.enable(false);
									bEliminar.enable(false);
									bGuardar.enable(false);

									tID.setText("");
									tNombre.setText("");
									tApellido.setText("");
									tEdad.setText("");
									zonaSelect.select(0);
									zonaSelect.enable(false);

								}

							} catch (SQLException e2) {
								e2.printStackTrace();
							}

						} else {
							JOptionPane
									.showMessageDialog(null,
											"Debe completar con el ID de Vendedor a buscar!!");
						}
					}
					if (rAdministrativo.isSelected()) {
						if (!tBuscar.getText().equals("")) {
							// BUSCAR
							// JOptionPane.showMessageDialog(null,
							// "Buscando ID");

							String databaseUrl = "jdbc:mysql://localhost:3306/rrhh";
							ConnectionSource connectionSource;
							try {
								connectionSource = new JdbcConnectionSource(
										databaseUrl);
								((JdbcConnectionSource) connectionSource)
										.setUsername("root");
								((JdbcConnectionSource) connectionSource)
										.setPassword("");

								admbus = DaoManager.createDao(connectionSource,
										Administrativo.class);

								// Administrativo resul =
								// admbus.queryForId(Integer.parseInt(tBuscar.getText()));
								// Esto funciona pero si no encuentra nada, da
								// ERROR y no encontre otra forma de evitarlo
								// que poniendolo en el IF de abajo
								if (admbus.queryForId(Integer.parseInt(tBuscar
										.getText())) != null) {
									Administrativo resul = admbus.queryForId(Integer
											.parseInt(tBuscar.getText()));
									//Como hacer para no repetir la busqueda, 
									// es decir como paso el registro encontrado
									
									tID.setText(Integer.toString(resul.getId()));
									tNombre.setText(resul.getNombre());
									tApellido.setText(resul.getApellido());
									tEdad.setText(Integer.toString(resul
											.getEdad()));
									bModificar.enable(true);
									bEliminar.enable(true);
								} else {
									JOptionPane.showMessageDialog(null,
											"no se encontraron registros");
									bModificar.enable(false);
									bEliminar.enable(false);
									bGuardar.enable(false);

									tID.setText("");
									tNombre.setText("");
									tApellido.setText("");
									tEdad.setText("");

								}
							} catch (SQLException e2) {
								e2.printStackTrace();
							}

						} else {
							JOptionPane
									.showMessageDialog(null,
											"Debe ingresar completar con un ID a buscar");
						}
					}
				} else {
					JOptionPane.showMessageDialog(null,
							"Debe seleccionar una opci�n de b�squeda");
				}
			}
		});
		bBuscar.setBounds(306, 49, 82, 22);
		contentPane.add(bBuscar);

		tID.setEnabled(false);
		tID.setBounds(39, 113, 61, 27);
		contentPane.add(tID);

		tNombre.setEnabled(false);
		tNombre.setBounds(106, 113, 142, 27);
		contentPane.add(tNombre);

		tApellido.setEnabled(false);
		tApellido.setBounds(254, 113, 142, 27);
		contentPane.add(tApellido);

		tEdad.setEnabled(false);
		tEdad.setBounds(402, 113, 82, 27);
		contentPane.add(tEdad);

		bModificar.setEnabled(false);
		bModificar.setBounds(206, 163, 70, 22);
		contentPane.add(bModificar);

		bGuardar.setEnabled(false);
		bGuardar.setBounds(282, 163, 70, 22);
		contentPane.add(bGuardar);

		label_1.setBounds(38, 82, 62, 22);
		contentPane.add(label_1);

		label_2.setBounds(118, 82, 62, 22);
		contentPane.add(label_2);

		label_3.setBounds(264, 82, 62, 22);
		contentPane.add(label_3);

		label_4.setBounds(410, 82, 62, 22);
		contentPane.add(label_4);

		buttonGroup.add(rVendedor);
		rVendedor.setBounds(98, 20, 112, 23);
		contentPane.add(rVendedor);

		buttonGroup.add(rAdministrativo);
		rAdministrativo.setBounds(226, 20, 134, 23);
		contentPane.add(rAdministrativo);

		label.setBounds(10, 20, 82, 22);
		contentPane.add(label);

		zonaSelect.enable(false);
		zonaSelect.addItem("---");
		zonaSelect.addItem("Norte");
		zonaSelect.addItem("Sur");
		zonaSelect.addItem("Este");
		zonaSelect.addItem("Oeste");
		zonaSelect.addItem("Centro");
		zonaSelect.setBounds(490, 113, 148, 27);
		contentPane.add(zonaSelect);

		bEliminar.setEnabled(false);
		bEliminar.setBounds(414, 163, 70, 22);
		contentPane.add(bEliminar);
		
		Button bCancelar = new Button("Cancelar");
		bCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VisualModiPersonal.this.dispose();
			}
		});
		bCancelar.setBounds(552, 198, 70, 22);
		contentPane.add(bCancelar);
	}
}
