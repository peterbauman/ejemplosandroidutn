package Clases;

import com.j256.ormlite.field.DatabaseField;

public class Administrativo extends Personal {
	@DatabaseField
	private String sector;

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public Administrativo() {

	}

	public Administrativo(int Id, String Nombre, String Apellido, int Edad,
			String Sector) {
		super();
		this.setId(Id);
		this.setNombre(Nombre);
		this.setApellido(Apellido);
		this.setEdad(Edad);
		this.sector = Sector;
	}

	@Override
	public String toString() {
		return "*******" + " " + getId() + " " + getNombre() + " "
				+ getApellido() + " " + getEdad() + " " + sector;
	}
}
