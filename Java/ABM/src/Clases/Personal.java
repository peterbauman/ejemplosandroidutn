package Clases;

import com.j256.ormlite.field.DatabaseField;

public class Personal {
	@DatabaseField
	private String nombre;
	@DatabaseField
	private String apellido;
	@DatabaseField
	private int edad;
	@DatabaseField (generatedId = true)
	private int id;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
