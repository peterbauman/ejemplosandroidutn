package Clases;

import com.j256.ormlite.field.DatabaseField;

public class Vendedor extends Personal {
	
	@DatabaseField
	private String zona;

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public Vendedor(String Nombre, String Apellido, int Edad, String Zona) {
		this.setNombre(Nombre);
		this.setApellido(Apellido);
		this.setEdad(Edad);
		this.zona = Zona;
	}

	public Vendedor() {
	}

	@Override
	public String toString() {
		return "*******" + " " + getId() + " " + getNombre() + " "
				+ getApellido() + " " + getEdad() + " " + zona;
	}
}
