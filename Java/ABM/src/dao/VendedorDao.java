package dao;

import java.sql.SQLException;

import Clases.Vendedor;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class VendedorDao extends BaseDaoImpl<Vendedor, String> implements
		VendedorDaoInterface {
	Dao<Vendedor, String> vendedorDao;

	VendedorDao(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Vendedor.class);
	}

	public void modificarDatosBD(ConnectionSource connectionSource) throws SQLException {
		vendedorDao = DaoManager.createDao(connectionSource, Vendedor.class);
		TableUtils.createTable(connectionSource, Vendedor.class);

	}
}
