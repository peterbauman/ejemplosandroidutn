package dao;

import java.sql.SQLException;

import Clases.Administrativo;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class AdministrativoDao extends BaseDaoImpl<Administrativo, String>
		implements AdministrativoDaoInterface {
	Dao<Administrativo, String> accountDao;

	public AdministrativoDao(ConnectionSource connectionSource)
			throws SQLException {
		super(connectionSource, Administrativo.class);
	}

	public void modificarDatosBD(ConnectionSource connectionSource)
			throws SQLException {
		accountDao = DaoManager.createDao(connectionSource,
				Administrativo.class);

		TableUtils.createTableIfNotExists(connectionSource,
				Administrativo.class);

	}

}
