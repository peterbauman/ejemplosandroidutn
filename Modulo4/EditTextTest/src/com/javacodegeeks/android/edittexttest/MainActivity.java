package com.javacodegeeks.android.edittexttest;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

public class MainActivity extends Activity {

	private EditText input, digits, pass, phone;
	private Button next, display;
	private Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context = this.getApplicationContext();
		
		input = (EditText) findViewById(R.id.editInp);
		digits = (EditText) findViewById(R.id.editDig);
		pass = (EditText) findViewById(R.id.editPass);
		phone = (EditText) findViewById(R.id.editPhone);
		
		phone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast msg = Toast.makeText(getBaseContext(), "Only 10 numbers",
						Toast.LENGTH_LONG);
				msg.show();
			}
		});
		
		next = (Button) findViewById(R.id.nextBtn);
				
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent myIntent = new Intent(context, NextScreen.class);
				myIntent.putExtra("pass", pass.getText().toString());
				myIntent.putExtra("phone", phone.getText().toString());
				startActivity(myIntent);
			}
		});
		
		display = (Button) findViewById(R.id.displayBtn);
		
		display.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				input.setTextColor(Color.RED);
				String displayString = "You typed '" + input.getText().toString() +
						"' as input text and '" + digits.getText().toString() + "' as digits";
				
				Toast msg = Toast.makeText(getBaseContext(), displayString,
						Toast.LENGTH_LONG);
				msg.show();
			}
		});
	}

}
