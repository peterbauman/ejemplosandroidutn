package com.javacodegeeks.android.uicontroltest;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ImageButton imageBtn = (ImageButton) findViewById(R.id.imageButton1);

	}
	
	public void clickButton(View v) {
		Intent intent = new Intent(getApplicationContext(), ActivityTwo.class);
	    startActivity(intent);
	}
	
}