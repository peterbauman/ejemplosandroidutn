package news.amalgame.data;

import java.sql.SQLException;
import java.util.List;

import news.amalgame.entity.Categorie;
import news.amalgame.entity.Customer;
import news.amalgame.entity.OrderProduct;
import news.amalgame.entity.Orders;
import news.amalgame.entity.Product;
import android.content.Context;

import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.stmt.DeleteBuilder;

public class DatabaseManager {
	static private DatabaseManager databaseManager;
	private DataSQLiteHelper dataSQLiteHelper;

	static public void init(Context ctx) {
		if (null == databaseManager) {
			databaseManager = new DatabaseManager(ctx);
		}
	}

	static public DatabaseManager getInstance() {
		return databaseManager;
	}

	private DatabaseManager(Context ctx) {
		dataSQLiteHelper = new DataSQLiteHelper(ctx);
	}

	private DataSQLiteHelper getHelper() {
		return dataSQLiteHelper;
	}

	// #######################################################################################
	// CATEGORIE
	// #######################################################################################

	public List<Categorie> getAllCategories() {
		List<Categorie> list = null;
		try {
			list = getHelper().getCategorieDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();

		}

		return list;
	}

	public void addcategorie(Categorie c) {
		try {
			getHelper().getCategorieDao().create(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteCategoryById(int ID) {
		try {
			getHelper().getCategorieDao().deleteById(ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Categorie getItemByName(String name) {
		try {
			List<Categorie> list = null;
			list = getHelper().getCategorieDao().queryForEq("name", name);
			return list.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateCategorie(Categorie p) {
		boolean ret = false;
		try {
			ret = getHelper().updateCategory(p);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}

	// #######################################################################################
	// Customer
	// #######################################################################################
	public List<Customer> getAllCustomer() {
		List<Customer> customerList = null;
		try {
			customerList = getHelper().getCustomerDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return customerList;
	}

	public void addCustomer(Customer c) {
		try {
			getHelper().getCustomerDao().create(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteCustomerByID(int iD) {
		try {
			getHelper().getCustomerDao().deleteById(iD);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Customer getCustomerByID(int customerId) {
		Customer p = null;

		try {
			p = getHelper().getCustomerDao().queryForId(customerId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return p;
	}

	public boolean updateCustomer(Customer p) {
		boolean ret = false;
		try {
			ret = getHelper().updateCustomer(p);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		return ret;

	}

	// #######################################################################################
	// Order Product
	// #######################################################################################

	public List<OrderProduct> getAllOrderProduct() {
		List<OrderProduct> list = null;
		try {
			list = getHelper().getOrderProductDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public void addOrderProduct(OrderProduct p) {
		try {
			getHelper().getOrderProductDao().create(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteOrderProduct(int ID) {
		try {
			getHelper().getOrderProductDao().deleteById(ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public OrderProduct getOrderProductByID(int productId) {
		OrderProduct p = null;

		return p;
	}

	public boolean updateOrderProduct(OrderProduct p) {
		boolean ret = false;
		try {
			ret = getHelper().updateOrderProduct(p);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;

		}
		return ret;
	}

	// #######################################################################################
	// ORDERS
	// #######################################################################################

	

	
	public List<Orders> getAllOrders() {
		List<Orders> list = null;
		try {
			list = getHelper().getOrdersDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void deleteOrder(int ID) {
		try {
			getHelper().getOrdersDao().deleteById(ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public boolean updateOrder(Orders p) {
		boolean ret = false;
		try {
			ret = getHelper().updateOrder(p);
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;

		}
		return ret;
	}
	
	// #######################################################################################
	// PRODUCT
	// #######################################################################################

	
    public int delete(Product product) throws SQLException {
        // first delete the clients that match the product's id
        DeleteBuilder db = getHelper().getOrderProductDao().deleteBuilder();
        db.where().eq("id", product.getId());
        getHelper().getProductDao().delete(db.prepare());
        // then call the super to delete the product
        return delete(product);
    }
	
	
	public List<Product> getAllproduct() {
		List<Product> list = null;
		try {
			list = getHelper().getProductDao().queryForAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void deleteProductByID(int iD) {
		try {
			getHelper().getProductDao().deleteById(iD);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addProduct(Product p) {
		try {
			getHelper().getProductDao().create(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean updateProduct(Product p) {
		boolean ret = false;
		CreateOrUpdateStatus createOrUpdateStatus;
		try {
			createOrUpdateStatus = getHelper().getProductDao()
					.createOrUpdate(p);
			if (createOrUpdateStatus.isUpdated()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		}
		return ret;
	}

	public Product getProductByID(int productId) {
		Product p = null;

		try {
			p = getHelper().getProductDao().queryForId(productId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return p;
	}

	/*
	 * public List<WishList> getAllWishLists() { List<WishList> wishLists =
	 * null; try { wishLists = getHelper().getWishListDao().queryForAll(); }
	 * catch (SQLException e) { e.printStackTrace(); } return wishLists; }
	 */
}
