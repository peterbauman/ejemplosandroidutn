package news.amalgame.data;

import news.amalgame.entity.Categorie;
import news.amalgame.entity.Customer;
import news.amalgame.entity.OrderProduct;
import news.amalgame.entity.Orders;
import news.amalgame.entity.Product;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DataSQLiteHelper extends OrmLiteSqliteOpenHelper {
	// Peter
	public static final String DATABASE_NAME_OK = "ventasdb.sqlite";
	public static final String DB_PATH_OK = "/sdcard/ventas/databases/";
	public static final String DATABASE_NAME_FULLPATH_OK = DB_PATH_OK
			+ DATABASE_NAME_OK;
	// Peter

	public static final String DATABASE_NAME = "ventasdb.db";
	public static final String DB_PATH = "/sdcard/ventas/databases/";
	public static final String DATABASE_NAME_FULLPATH = DB_PATH + DATABASE_NAME;
	// any time you make changes to your database objects, you may have to
	// increase the database version
	private static final int DATABASE_VERSION = 1;
	private Context mContext;

	// the DAO object we use to access the SimpleData table
	private Dao<Customer, Integer> customerDao;
	private Dao<Product, Integer> productDao;
	private Dao<Categorie, Integer> categorieDao;
	private Dao<Orders, Integer> OrdersDao;
	private Dao<OrderProduct, Integer> OrderProducDao;

	// private Dao<CustomerAccount, Integer> customerAccountDao;

	// private String SQLCREATE =
	// "CREATE TABLE [Customer] ([customerId] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[name] TEXT  NOT NULL,[cel] teXT  NULL,[email] teXT  NULL,[address] texT  NULL,[city] texT  NULL)";

	public DataSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource conections) {
		try {

			TableUtils.createTable(connectionSource, Customer.class);
			TableUtils.createTable(connectionSource, Product.class);
			TableUtils.createTable(connectionSource, Categorie.class);
			TableUtils.createTable(connectionSource, OrderProduct.class);
			TableUtils.createTable(connectionSource, Orders.class);

			/*
			 * Categorie c = new Categorie(0, "General"); DatabaseManager
			 * manager; manager = DatabaseManager.getInstance();
			 * manager.addcategorie(c);
			 */

		} catch (Exception e) {
			Log.e(DataSQLiteHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
			int oldVersion, int newVersion) {
		try {
			TableUtils.dropTable(connectionSource, Customer.class, true);
			TableUtils.dropTable(connectionSource, Product.class, true);
			TableUtils.dropTable(connectionSource, Categorie.class, true);
			TableUtils.dropTable(connectionSource, OrderProduct.class, true);
			TableUtils.dropTable(connectionSource, Orders.class, true);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Returns the Database Access Object (DAO) for our UserData class. It will
	 * create it or just give the cached value.
	 */
	public Dao<Customer, Integer> getCustomerDao() {
		if (customerDao == null) {
			try {
				customerDao = getDao(Customer.class);
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return customerDao;
	}

	public Dao<Product, Integer> getProductDao() {
		if (productDao == null) {
			try {
				productDao = getDao(Product.class);
			} catch (java.sql.SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return productDao;
	}

	public Dao<Categorie, Integer> getCategorieDao() {
		if (categorieDao == null) {
			try {
				categorieDao = getDao(Categorie.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return categorieDao;
	}

	public Dao<Orders, Integer> getOrdersDao() {
		if (OrdersDao == null) {
			try {
				OrdersDao = getDao(Orders.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return OrdersDao;
	}

	public Dao<OrderProduct, Integer> getOrderProductDao() {
		if (OrderProducDao == null) {
			try {
				OrderProducDao = getDao(OrderProduct.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return OrderProducDao;
	}

	public boolean updateProduct(Product p) {
		boolean ret = false;
		if (productDao != null) {
			try {
				productDao = getDao(Product.class);

				if (productDao.update(p) != 1) {
					ret = false;
				} else {
					productDao.refresh(p);
					ret = true;
				}

				productDao.refresh(p);

			} catch (Exception e) {
				ret = false;
				e.printStackTrace();
			}
		}
		return ret;
	}

	public boolean updateCustomer(Customer p) {
		boolean ret = false;
		if (customerDao != null) {
			try {
				customerDao = getDao(Customer.class);

				if (customerDao.update(p) != 1) {
					ret = false;
				} else {
					customerDao.refresh(p);
					ret = true;
				}

				customerDao.refresh(p);

			} catch (Exception e) {
				ret = false;
				e.printStackTrace();
			}
		}
		return ret;

	}

	public boolean updateOrder(Orders p) {
		boolean ret = false;
		if (OrdersDao != null) {
			try {
				OrdersDao = getDao(Orders.class);

				if (OrdersDao.update(p) != 1) {
					ret = false;
				} else {
					OrdersDao.refresh(p);
					ret = true;
				}

				OrdersDao.refresh(p);

			} catch (Exception e) {
				ret = false;
				e.printStackTrace();
			}
		}
		return ret;

	}

	public boolean updateOrderProduct(OrderProduct p) {
		boolean ret = false;
		if (OrderProducDao != null) {
			try {
				//OrderProducDao = getDao(Customer.class);

				if (OrderProducDao.update(p) != 1) {
					ret = false;
				} else {
					OrderProducDao.refresh(p);
					ret = true;
				}

				OrderProducDao.refresh(p);

			} catch (Exception e) {
				ret = false;
				e.printStackTrace();
			}
		}
		return ret;

	}

	public boolean updateCategory(Categorie p) {
		boolean ret = false;
		if (customerDao != null) {
			try {
				//customerDao = getDao(Categorie.class);

				if (categorieDao.update(p) != 1) {
					ret = false;
				} else {
					categorieDao.refresh(p);
					ret = true;
				}

				categorieDao.refresh(p);

			} catch (Exception e) {
				ret = false;
				e.printStackTrace();
			}
		}
		return ret;

	}

	public boolean deleteCustomer(Customer p) {
		DeleteBuilder db = productDao.deleteBuilder();
		try {
			db.where().eq("customerID", p.getId());
			customerDao.delete(db.prepare());
			this.deleteCustomer(p);
			return true;
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();

	}

}
