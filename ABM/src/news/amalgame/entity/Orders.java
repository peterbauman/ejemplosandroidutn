package news.amalgame.entity;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 * CREATE TABLE orders (
 id INTEGER PRIMARY KEY,
 customer_id INTEGER,
 date STRING,
 FOREIGN KEY(customer_id) REFERENCES customers(id)
 )

 */
@DatabaseTable(tableName = "Orders")
public class Orders {

	public Orders(int id, String date, int customer_id) {
		super();
		this.id = id;
		this.date = date;
		this.customer_id = customer_id;
	}

	@DatabaseField(generatedId = true, canBeNull = false)
	private int id;

	@DatabaseField(canBeNull = false)
	private String date;

	@DatabaseField(foreign = true, canBeNull = false)
	// refers to customers(id)
	private int customer_id;

	public Orders() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

}
