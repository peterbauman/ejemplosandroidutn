package amalgame.entity;

import java.util.List;

import amalgame.ventas.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter 	 extends ArrayAdapter<String> {

	    public static final int PRODUCT = 1;
		private Context mContext;
	    private int id;
	    private List <String>items ;
	    private int typeAdapter=0; //0-CUSTOMER 1-PRODUCT 
	    
	    @SuppressWarnings("unchecked")
		public CustomAdapter(Context context, List<String> list ) 
	    {
	        super(context, R.layout.simpleiconrow, list);           
	        mContext = context;
	       
	        items = list ;
	    }

	    public CustomAdapter(Context mContext2, List<String> list,int product2) {
	    	super(mContext2, R.layout.simpleiconrow, list);           
	        mContext = mContext2;
	       
	        items = list ;
	        typeAdapter=product2;
	        
		}

		@Override
	    public View getView(int position, View v, ViewGroup parent)
	    {
	        View mView = v ;
	        if(mView == null){
	            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            mView = vi.inflate(R.layout.simpleiconrow, null);
	        }

	        TextView text = (TextView) mView.findViewById(R.id.simpleiconrow_title);
	        ImageView img = (ImageView ) mView.findViewById(R.id.simpleiconrow_icon);

	        if(items.get(position) != null )
	        {
	            text.setTextColor(Color.BLACK);
	            text.setText(items.get(position));
	           
	            
	            if( typeAdapter==PRODUCT) {
	            	Drawable drawable = mContext.getResources().getDrawable(R.drawable.product);
	            	
	            	
	            	img.setImageDrawable(drawable);
	            }
	            
	            //int color = Color.argb( 200, 255, 64, 64 );	                text.setBackgroundColor( color );

	        }
	        
	        //v.setBackground(mContext.getResources().getDrawable(  R.drawable.fondolist));
	        
	        return mView;
	    }

	
}
