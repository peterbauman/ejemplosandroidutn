package amalgame.entity.db;

import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Customer")
// @DatabaseTable(daoClass = Customer.class)
public class Customer {


//	@ForeignCollectionField(eager = false)
//    private Collection<Product> products	;
	
	@DatabaseField(index = true, generatedId = true)
	private int customerID;

//	public Collection<Product> getProducts() {
//		return products;
//	}
//
//	public void setProducts(Collection<Product> products) {
//		this.products = products;
//	}

	@DatabaseField(canBeNull = false)
	private String name;
	@DatabaseField(canBeNull = true)
	private String cel;
	@DatabaseField(canBeNull = true)
	private String email;
	@DatabaseField(canBeNull = false)
	private String address;
	
	


	@DatabaseField(canBeNull = false)
	private String City;

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Customer() {
		// ORMLite needs a no-arg constructor
	}

	public Customer(int id, String name, String cel, String email,
			String address, String city) {
		super();
		this.customerID = id;
		this.name = name;
		this.cel = cel;
		this.email = email;
		this.address = address;
		this.City = city;
	}

	public void setId(int id) {
		this.customerID = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCel() {
		return cel;
	}

	public void setCel(String cel) {
		this.cel = cel;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

}
