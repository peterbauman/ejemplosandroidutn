package amalgame.entity.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Product")
public class Product {
	@DatabaseField(index = true, generatedId = true)
	private int productId;

	@DatabaseField
	private String name;

	@DatabaseField
	private int quantity;

	// @DatabaseField(canBeNull = true)
	// private Integer categorie;
	// http://logic-explained.blogspot.com.ar/2011/12/using-ormlite-in-android-projects.html

	@DatabaseField
	private int categorie;

	// @ForeignCollectionField
	// private ForeignCollection<Categorie> itemsCategorie;

	@DatabaseField
	private String description;

	@DatabaseField
	private String photo;
	
	@DatabaseField
	private int state; //0==false==inactivo==borrado ||  1 ==true == activo

	public Product() {
	}

	public Product(String name, int quantity, int categorie,
			String description, String photo,int state) {
		super();
		this.productId = productId;
		this.name = name;
		this.quantity = quantity;
		this.categorie = categorie;
		this.description = description;
		this.photo = photo;
		this.state = state;
	}

	
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return description;
	}

	public void setAddress(String address) {
		this.description = address;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCategorie() {
		return categorie;
	}

	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public CharSequence getDesc() {
		return this.description;
	}

}
