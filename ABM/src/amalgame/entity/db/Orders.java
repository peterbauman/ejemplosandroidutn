package amalgame.entity.db;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 * CREATE TABLE orders (
 id INTEGER PRIMARY KEY,
 customer_id INTEGER,
 date STRING,
 FOREIGN KEY(customer_id) REFERENCES customers(id)
 )

 */
@DatabaseTable(tableName = "Orders")
public class Orders {


	@DatabaseField(index=true,generatedId = true, canBeNull = false)
	private int id;

	@DatabaseField(canBeNull = false)
	private String date;

	@DatabaseField(canBeNull = false)
	private int customer_id;

	@DatabaseField(canBeNull = false)
	private int state;
	
	Orders() {
	}

	/**
	 * @param String date
	 * @param int customer_id
	 * @param int state 1=active , 0=inactive
	 */
	public Orders(String date, int customer_id,int state) {
		super();
		
		this.date = date;
		this.customer_id = customer_id;
		this.state = state;
	}

	
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}

	
	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

}
