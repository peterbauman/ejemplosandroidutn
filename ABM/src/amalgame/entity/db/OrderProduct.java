package amalgame.entity.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "OrderProduct")
public class OrderProduct {

	@DatabaseField(index=true,generatedId=true, canBeNull = false)
	private int id;
	
	@DatabaseField(canBeNull = false)
	private int order_id;

	@DatabaseField(canBeNull = false)
	private int product_id;
	
	@DatabaseField(canBeNull = false)
	private int product_quantity;

	OrderProduct(){}
	
	
	//new OrderProduct(order_selection ,product_selection.getProductId(), quantity);
	public OrderProduct(int order_selection, int productId, int quantity) {
		this.order_id=order_selection;
		this.product_id=productId;
		this.product_quantity=quantity;
		
	
	}

	
	

	public OrderProduct(int orderProductID, int quantity) {
		super();
		this.product_id = orderProductID;
		this.product_quantity = quantity;
	}






	public int getOrderProductID() {
		return id;
	}

	

	public int getQuantity() {
		return product_quantity;
	}

	public void setQuantity(int quantity) {
		this.product_quantity = quantity;
	}



	public int getProduct_id() {
		return product_id;
	}



	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	
	
	
}
