package amalgame.entity.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "Categorie")

public class Categorie {


	@DatabaseField(index = true, generatedId = true)
	private int categorieID;

	@DatabaseField(canBeNull = false)
	private String name;
	
	Categorie (){
	//constructor vacio necesario para ORM	
	}

	
	
	public Categorie(int categorieID, String name) {
		super();
		this.categorieID = categorieID;
		this.name = name;
	}



	public int getId() {
		return categorieID;
	}

	public void setId(int productId) {
		this.categorieID = productId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	
	
}
