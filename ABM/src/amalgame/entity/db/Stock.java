package amalgame.entity.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "Stock")
public class Stock {

	@DatabaseField(index=true,generatedId = true, canBeNull = false)
	private int id;

	@DatabaseField(canBeNull = false)
	private String movement_date;

	@DatabaseField(canBeNull = false)
	private int product_id;

	@DatabaseField(canBeNull = false)
	private int quantity;
	
	
	Stock() {
	}

	public Stock(String movement_date, int product_id,int quantity) {
		super();
		
		this.movement_date = movement_date;
		this.product_id = product_id;
		this.quantity = quantity;
	}

	public String getMovement_date() {
		return movement_date;
	}

	public void setMovement_date(String movement_date) {
		this.movement_date = movement_date;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	
	
	
}
