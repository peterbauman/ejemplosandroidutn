package amalgame.entity;

import amalgame.ventas.MainActivity;
import amalgame.ventas.R;
import amalgame.ventas.AboutusActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.FeatureInfo;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;



	


public class Actividad extends Activity{
	

	public static boolean cerrarAplicacion;
	private static boolean custom;
    public static final int MENU_SALIR= Menu.FIRST;

    public static final int MENU_ACERCADE= Menu.FIRST+1;

	public static boolean desconectadoDelServidor = false;
    public static boolean appRunning = false;
	public static boolean islogueado = false;
	public static int sat_count;
	public static boolean enviarLoginDesconectado ;
	public static int NOTIFICATION = android.R.string.yes;
	public static NotificationManager mNM;
    public static String TAG = "ACTIVIDAD";
    Intent intent;
    static final int NOTIFICATION_ID = 1593;

    public Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		
				if (!isTaskRoot()) {
	 
					final Intent intent = getIntent();
				    final String intentAction = intent.getAction(); 
				    if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
				        Log.i(TAG, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
				        finish();
				        return;       
				    }
				}

					
			//	setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	
		//getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg3));
		custom = customTitleSupported;
		mContext = getApplicationContext();
		
		       	 
						 
					
	}

	
	@Override
	public void setContentView(int id) {

	
		try {
			super.setContentView(id);		
			setTitle();	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("null")
	public static void ActivarMenuItem(int Opsel){				
		MenuItem menu = null;
		switch (Opsel) {					
			case MENU_ACERCADE:
				((MenuItem) menu).setEnabled(true);
				
		}
	}
	
	public void setTitle(){
		
		if ( custom ){	
			//getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, 0);	        
			//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.);			
		}
		
	}
	public  void viewExit(){
		
		AlertDialog dialog = new AlertDialog.Builder(Actividad.this)
	    .setMessage(getString(R.string.do_you_want_to_quit_))
	    .setPositiveButton(getString(R.string.si), new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) { 		
	     		Log.i(TAG," parando servicio: geoloc2 |");        	
	        	//###############################################################################################################################################
	        	// PARANDO SERVICIOS ######################################################
		        	try{
		        		Actividad.mNM.cancel(NOTIFICATION);
		        	}catch (Exception e) {
		        		Log.i(TAG,"Error parando servicio: geoloc2 |" + e.getMessage() );
		    		}
		        //###############################################################################################################################################
	            //###############################################################################################################################################	           	
	            finish();
	        }
	    })
	    .setNegativeButton(getString(R.string.no), null).create();
		dialog.show();
		
		
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();   	
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	       menu.add(0, MENU_SALIR, 0, getResources().getString(R.string.exit)).setIcon(android.R.drawable.ic_lock_power_off);
	      // menu.add(0, MENU_INFOCEL, 0,  getResources().getString(R.string.estado)).setIcon(R.drawable.mobilephone);
	       menu.add(0, MENU_ACERCADE, 0,  getResources().getString(R.string.about_us)).setIcon(android.R.drawable.ic_menu_info_details);	    
	
	       return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menu) {
		Intent intent;
			switch (menu.getItemId()) {
			
				case MENU_SALIR:
					viewExit();
					return true;					
				case MENU_ACERCADE:
					intent = new Intent(getApplicationContext(), AboutusActivity.class);
					startActivity(intent);					
					return true;				
			
			}
			return false;
		}
	
	
	
	
}
