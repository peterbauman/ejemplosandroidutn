package amalgame.entity;


import amalgame.ventas.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Util extends Activity {
	static Context context;
	public enum ButtonStyleDialog { OK, OK_CANCEL, CANCEL }
	public enum ProcessTypeShowDialogElim { CUSTOMER,PRODUCT,CATEGORIE }
	
	
	private String processTypeShowDialogElim;
	
	private Dialog customDialog;
	
	
	
	
	
	static private Util instance;

	static public void init(Context ctx){
		if (null==instance)
			instance = new Util(ctx);
	}
	
	
	private Util (Context ctx){
		context = ctx;
	}
	
	
	static public Util getInstanceUtil(){
		return instance;
	}
	
	/**
	 * @param String message
	 * @param String title
	 * @return 
	 */
	public Dialog showDialog(String message, String title,Context mContext){

			
			customDialog = new Dialog(mContext);
			customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);			
			customDialog.setContentView(R.layout.simpledialog);
			customDialog.setCancelable(false);		
			
			TextView tv_title =  (TextView) customDialog.findViewById(R.id.simpledialog_textview_title);
			TextView tv_text  = (TextView) customDialog.findViewById(R.id.simpledialog_textview_text);
			Button btn_ok     = (Button) customDialog.findViewById(R.id.simpledialog_ok);
		
			tv_title.setText(title);
			tv_text.setText(message);
			
			
			
			//btn_cancel.setOnClickListener(new OnClickListener() {
			//	@Override
			//	public void onClick(View v) {}
			//});
			btn_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					customDialog.cancel();
					//finish();
				}
			});
				
			return customDialog;
	}
	
	

	public static boolean checkExternalMedia() {

		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else if (Environment.MEDIA_SHARED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need to know is we can neither read nor write
			Log.i("err", "State=" + state + " Not good");
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		Log.i("UTIL", "Available=" + mExternalStorageAvailable + " Writeable="
				+ mExternalStorageWriteable + " State" + state);

		return (mExternalStorageAvailable && mExternalStorageWriteable);
	}

}
