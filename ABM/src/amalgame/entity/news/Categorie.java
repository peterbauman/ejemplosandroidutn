package amalgame.entity.news;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 * @DatabaseField(foreign = true, canBeNull = true)
 private InstallationType installationType;
 */
@DatabaseTable(tableName = "Categories")
public class Categorie {
/*
 * CREATE TABLE Product(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  quantity INTEGER NOT NULL, 
  description TEXT,
  photo TEXT,
  categorie INTEGER NOT NULL,  
  FOREIGN KEY (categorie) REFERENCES categorieProducts(Id)
)
 */

	@DatabaseField(generatedId = true, canBeNull = false)
	private int id;

	public Categorie(int id, String name, String cel, String email,
			String address, String city) {
		super();
		this.id = id;
		this.name = name;
		this.cel = cel;
		this.email = email;
		this.address = address;
		this.city = city;
	}

	@DatabaseField(canBeNull = false)
	private String name;

	@DatabaseField
	private String cel;

	@DatabaseField
	private String email;

	@DatabaseField
	private String address;

	@DatabaseField
	private String city;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCel() {
		return cel;
	}

	public void setCel(String cel) {
		this.cel = cel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}