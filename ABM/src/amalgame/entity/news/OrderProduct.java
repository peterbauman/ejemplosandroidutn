package amalgame.entity.news;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 *   id INTEGER PRIMARY KEY,
 order_id INTEGER,
 product_id INTEGER,
 product_quantity INTEGER,
 FOREIGN KEY(order_id) REFERENCES orders(id),
 FOREIGN KEY(product_id)
 */
@DatabaseTable(tableName = "OrderProduct")
public class OrderProduct {

	public OrderProduct(int id, int product_quantity, int order_id,
			int product_id) {
		super();
		this.id = id;
		this.product_quantity = product_quantity;
		this.order_id = order_id;
		this.product_id = product_id;
	}

	@DatabaseField(generatedId = true, canBeNull = false)
	private int id;

	@DatabaseField(canBeNull = false)
	int product_quantity;

	@DatabaseField(foreign = true, canBeNull = false)
	// //REFERENCES orders(id)
	private int order_id;

	@DatabaseField(foreign = true, canBeNull = false)
	// REFERENCES product(id)
	private int product_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProduct_quantity() {
		return product_quantity;
	}

	public void setProduct_quantity(int product_quantity) {
		this.product_quantity = product_quantity;
	}

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

}
