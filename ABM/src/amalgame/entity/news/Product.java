package amalgame.entity.news;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
 * @DatabaseField(foreign = true, canBeNull = true)
 private InstallationType installationType;
 */
@DatabaseTable(tableName = "Product")
public class Product {
	@DatabaseField(generatedId = true, canBeNull = false)
	private int id;

	@DatabaseField(canBeNull = false)
	private String name;

	@DatabaseField(canBeNull = false)
	private int quantity;

	@DatabaseField
	private String description;

	@DatabaseField
	private String photo;

	@DatabaseField(foreign = true, canBeNull = false)
	// refers to categorieProducts(Id)
	private int categorie;

	
	public Product() {
	}

	public Product(int id, String name, int quantity, String description,
			String photo, int categorie) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.description = description;
		this.photo = photo;
		this.categorie = categorie;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return description;
	}

	public void setAddress(String address) {
		this.description = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getCategorie() {
		return categorie;
	}

	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public CharSequence getDesc() {
		return this.description;
	}

}
