package amalgame.ventas;

import java.util.ArrayList;

import amalgame.data.DataSQLiteHelper;
import amalgame.data.DatabaseManager;
import amalgame.entity.Actividad;
import amalgame.entity.Util;
import amalgame.entity.db.Customer;
import amalgame.entity.db.Stock;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;

public class MainActivity extends Actividad {
	private static final String TAGS = null;
	private String[] datos = new String[6];
	private GridView grdOpciones;
	private ArrayAdapter<String> adaptador;
	private ArrayList<MenuItems> arrListItems;
	private Context mContext;
	public int NOTIFICATION = android.R.string.yes;
	private Activity[] activities;
	private Util utilman;
	private DataSQLiteHelper dataSQLiteHelper = null;
	private RuntimeExceptionDao<Customer, Integer> daoCustomer;
	private Dao<Customer, Integer> customerDao;

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		grdOpciones = (GridView) findViewById(R.id.gridopciones);

		mContext = getApplicationContext();
		Actividad.mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		showNotification();

		arrListItems = new ArrayList<MenuItems>();
		final String titulos[] = { 
				"Customer","Products",
				getString(R.string.customer_order),
				getString(R.string.categoryproducts),
				getString(R.string.stock), 
				getString(R.string.info)
		};

		MenuItems mitem = new MenuItems("", "");

		for (int x = 0; x < titulos.length; x++) {
			//mitem = new MenuItems("", titulos[x]);
			arrListItems.add(new MenuItems("", titulos[x]));

		}

		grdOpciones.setAdapter(new AdaptadorTitulares(this, arrListItems));

		grdOpciones.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				try {
					
		
					Toast.makeText(mContext,
							arrListItems.get(position).getTitulo(),
							Toast.LENGTH_SHORT).show();

					if (getString(R.string.customer) == arrListItems.get(
							position).getTitulo()) {
						
						
						Intent intent = new Intent();
						intent.setClass(mContext, CustomerActivity.class);
						startActivity(intent);
						
						
					}

					if (getString(R.string.products) == arrListItems.get(
							position).getTitulo()) {
						Intent intent = new Intent();
						intent.setClass(mContext, ProductActivity.class);
						startActivity(intent);
					}

					if (getString(R.string.customer_order) == arrListItems.get(
							position).getTitulo()) {

						Intent intent = new Intent();
						intent.setClass(mContext, OrdersActivity.class);
						startActivity(intent);
					}

					if (getString(R.string.categoryproducts) == arrListItems.get(
							position).getTitulo()) {

						Intent intent = new Intent();
						intent.setClass(mContext, CategoriesActivity.class);
						startActivity(intent);
					}
					
					if (getString(R.string.stock) == arrListItems.get(
							position).getTitulo()) {

						Intent intent = new Intent();
						intent.setClass(mContext, StockActivity.class);
						startActivity(intent);
					}

					
					if (getString(R.string.info) == arrListItems.get(position).getTitulo()) {

						Intent intent = new Intent();
						intent.setClass(mContext, AboutusActivity.class);
						startActivity(intent);
					}

					// Intent intent = new Intent(); intent.setClass(mContext,
					// ProductOrderActivity.class); startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		try {

			// get our dao
			// Dao<Customer, Integer> simpleDao = getHelper().getDao();

			// dataSQLiteHelper = getHelper();
			// customerDao = dataSQLiteHelper.getCustomerDao();

			DatabaseManager.init(mContext);
			Util.init(mContext);

			/*
			 * Customer customer = new Customer(0, "juan", "423589",
			 * "juan@gmail.com", "cuchacucha 232", "rosario");
			 * customerDao.create(customer);
			 * 
			 * 
			 * // query for all of the data objects in the database
			 * List<Customer> list = customerDao.queryForAll(); // our string
			 * builder for building the content-view StringBuilder sb = new
			 * StringBuilder();
			 * sb.append("got").append(list.size()).append(" entries in "
			 * ).append("user act").append("\n");
			 * 
			 * // if we already have items in the database int simpleC = 0; for
			 * (Customer user : list) { sb.append("覧覧覧覧覧覧覧\n");
			 * sb.append(
			 * "[").append(simpleC).append("] = ").append("Name: "+user
			 * .getName()+". city: "+user.getCity()).append("\n");
			 * System.out.println(sb.toString()); simpleC++; }
			 */

		} catch (SQLException e) {
			Log.e(TAGS, "Database exception", e);
			// tv.setText(泥atabase exeption: � + e);
			return;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public class AdaptadorTitulares extends BaseAdapter {
		Activity context;
		private DataSQLiteHelper userHelper = null;
		private ArrayList<MenuItems> arrayListMenuItems;

		AdaptadorTitulares(Activity context,
				ArrayList<MenuItems> arrayListMenuItems) {
			// super(context, R.layout.iconrow, datos);
			this.context = context;
			this.arrayListMenuItems = arrayListMenuItems;

		}

		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View gridView;
			if (convertView == null) {

				gridView = new View(context);

				gridView = inflater.inflate(R.layout.iconrow, null);

				// set value into textview
				TextView textView = (TextView) gridView
						.findViewById(R.id.iconrow_title);
				textView.setText(arrayListMenuItems.get(position).getTitulo());

				Drawable iconDrawable = null;
				if (getString(R.string.customer) == arrayListMenuItems.get(
						position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.customer_on);
				}
				if (getString(R.string.products) == arrayListMenuItems.get(
						position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.products_on);
				}
				if (getString(R.string.customer_order) == arrayListMenuItems
						.get(position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.customer_order_on);
				}
				
				if (getString(R.string.categoryproducts ) == arrayListMenuItems
						.get(position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.category_product_on);
				}
				
				if (getString(R.string.stock ) == arrayListMenuItems
						.get(position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.search);
				}
				

				if (getString(R.string.info ) == arrayListMenuItems
						.get(position).getTitulo()) {
					iconDrawable = context.getResources().getDrawable(
							R.drawable.info);
				}
				
				
				ImageView imageView = (ImageView) gridView
						.findViewById(R.id.iconrow_icon);

				imageView.setImageDrawable(iconDrawable);

			} else {
				gridView = (View) convertView;
			}

			return gridView;
		}

		@Override
		public int getCount() {
			return arrayListMenuItems.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	class MenuItems {
		String icono;
		String titulo;

		public MenuItems(String icono, String titulo) {
			super();
			this.icono = icono;
			this.titulo = titulo;
		}

		public String getIcono() {
			return icono;
		}

		public void setIcono(String icono) {
			this.icono = icono;
		}

		public String getTitulo() {
			return titulo;
		}

		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}

	}

	/**
	 * You値l need this in your class to get the helper from the manager once
	 * per class.
	 */
	private DataSQLiteHelper getHelper() {
		if (dataSQLiteHelper == null) {

			dataSQLiteHelper = OpenHelperManager.getHelper(
					getApplicationContext(), DataSQLiteHelper.class);
		}
		return dataSQLiteHelper;
	}

	/*
	 * --------------------------------------------------------------------------
	 * ------------------------------------------------- ANULADO EL BOTON VOLVER
	 * --
	 * ------------------------------------------------------------------------
	 * -------------------------------------------------
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			viewExit();
		}
		return false;
	}

	public void showNotification() {
		try {
			String titulo = getString(amalgame.ventas.R.string.ventas_)
					.toString();
			String Descripcion = getString(R.string.tap_to_open);

			Intent intent = new Intent();
			// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.setClass(getApplicationContext(), MainActivity.class);

			PendingIntent contentIntent = PendingIntent.getActivity(
					getApplicationContext(), 0, intent, 0);
			Notification notification = new Notification(
					R.drawable.products_on, titulo, System.currentTimeMillis());

			notification.flags |= Notification.FLAG_ONGOING_EVENT;
			notification.setLatestEventInfo(this, titulo, Descripcion,
					contentIntent);
			Actividad.mNM.notify(NOTIFICATION, notification);
			// notifManager.notify(NOTIFICATION, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
