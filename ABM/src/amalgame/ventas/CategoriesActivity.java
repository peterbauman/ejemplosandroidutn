package amalgame.ventas;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import amalgame.data.DataSQLiteHelper;
import amalgame.data.DatabaseManager;
import amalgame.entity.Actividad;
import amalgame.entity.CustomAdapter;
import amalgame.entity.ListaActividad;
import amalgame.entity.db.Categorie;
import amalgame.entity.db.Customer;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.j256.ormlite.dao.Dao;

public class CategoriesActivity extends ListaActividad{
	  ListView listView;
	  List<Categorie> categorieList ;
	  List<String> categorieListStr; 
	  Dao<Categorie, Integer> categorieDao;
	  DataSQLiteHelper dataSQLiteHelper;
	  private ImageButton	  btn_ok;

	  private DatabaseManager manager;
	  private Context mContext;
	  private Dialog customDialog;
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.abm_categories);

		Random r =  new Random();
		int Max = 100;
		int Min = 1;
		int ran = r.nextInt(Max-Min+1)+Min ;
		mContext = getApplicationContext();
		
		
		btn_ok = (ImageButton) findViewById(R.id.abm_categories_add);
		
		
		
		btn_ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
					
				customDialog = showDialog("Add a categorie", CategoriesActivity.this);
				customDialog.show();
					
			}
		});
		
		
		 manager = DatabaseManager.getInstance();
		 
		 
		 

		 updateCustomerList_customAdapter();
		
		
		//##################################################################################################################
		 // delete
		 //##################################################################################################################		 
		 listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
					try {
						if(categorieList.size()<=0) return;
						
						
						// ITEM
						int id  = categorieList.get(position).getId();
						String name =  categorieList.get(position).getName();
						   		
						//TITTLE & TEXT
						String  title = getResources().getString(R.string.categoryproducts);
						String text =  getResources().getString ( R.string.do_you_want_to_delete_this_item_) + " " + name;
						
						Dialog dialog = new Dialog(CategoriesActivity.this);
						dialog = showDialogEliminar(text, title,   id);
						dialog.show();
						
				
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		
	}
	
	
	/**
	 * @param String message
	 * @param String title
	 * @return 
	 */
	public Dialog showDialog( String title,final Context mContext){

			
			customDialog = new Dialog(mContext);
			customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);			
			customDialog.setContentView(R.layout.simpledialog_textedit);
			customDialog.setCancelable(false);		
			
			TextView tv_title =  (TextView) customDialog.findViewById(R.id.simpledialog_edittext_title);
			final TextView ed_text  = (TextView) customDialog.findViewById(R.id.simpledialog_edittext_desc);
			Button btn_ok     = (Button) customDialog.findViewById(R.id.simpledialog_edittext_ok);
			Button btn_cancel     = (Button) customDialog.findViewById(R.id.simpledialog_edittext_cancel);
		
			tv_title.setText(title);
						
			btn_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					String desc = ed_text.getText().toString().trim();
					
					if(!desc.equals("")){
						if (desc.length()>0 && desc.length()<20){
							
							Categorie c = new Categorie(0, desc);
							
							Categorie c_tmp = manager.getItemByName(c.getName());
							if(c_tmp==null){
								manager.addcategorie(c);
								updateCustomerList_customAdapter();
								customDialog.cancel();
							}else{
								Toast.makeText(mContext,getString( R.string.the_item_already_exists_ ),Toast.LENGTH_LONG).show();
							}													
						}
					}else{
						Toast.makeText(mContext,getString( R.string.you_must_insert_a_value_to_continue),Toast.LENGTH_LONG).show();
					}
			
				}
			});
			btn_cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					customDialog.cancel();
					//finish();
				}
			});
			
			return customDialog;
	}
	
	
	
	/**
	 * @param String message
	 * @param String title
	 * @return 
	 */
	public Dialog showDialogEliminar(String message, String title,final int ID){
			//
			
			customDialog = new Dialog(CategoriesActivity.this);
			customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);			
			customDialog.setContentView(R.layout.simpledialog_okcancel);
			customDialog.setCancelable(false);		
			
			TextView tv_title =  (TextView) customDialog.findViewById(R.id.simpledialog_okcancel_title);
			TextView tv_text  = (TextView) customDialog.findViewById(R.id.simpledialog_okcancel_text);
			Button btn_ok     = (Button) customDialog.findViewById(R.id.simpledialog_okcancel_ok);
			Button btn_cancel     = (Button) customDialog.findViewById(R.id.simpledialog_okcancel_cancel);
		
			tv_title.setText(title);
			tv_text.setText(message);

		
			btn_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					manager.deleteCategoryById(ID);
					updateCustomerList_customAdapter();
					customDialog.cancel();
					//finish();
				}
			});
			
			btn_cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					customDialog.cancel();
					//finish();
				}
			});
			return customDialog;
	}			
			
			
			
	
	private void updateCustomerList_customAdapter(){
		categorieList = manager.getAllCategories();
		categorieListStr = new ArrayList<String>();
		listView = (ListView) getListView();
		
		if (categorieList ==null){	
			categorieListStr.add( getString(R.string.no_item_added)  );
			unregisterForContextMenu(listView);
		}else{
			for (Categorie item : categorieList) {
				categorieListStr.add(item.getName());
			}
			registerForContextMenu(listView);
		}
		
		CustomAdapter customAdapter = new CustomAdapter(mContext, categorieListStr);

		listView.setAdapter(customAdapter);

		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateCustomerList_customAdapter();
	}
	
}
