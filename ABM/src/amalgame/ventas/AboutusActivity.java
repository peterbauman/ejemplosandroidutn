package amalgame.ventas;

import amalgame.entity.Actividad;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AboutusActivity extends Actividad{

	public String versionCode;
	public Intent intent;
	private Context mContext;
	private ImageButton rateit;
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		
		
		MenuItem item;
		item = menu.findItem(Actividad.MENU_SALIR);
		item.setVisible(false);
		item = menu.findItem(Actividad.MENU_ACERCADE);
		item.setVisible(false);
		
		//MenuItem item;

		//item = menu.findItem(Actividad.MENU_INFOCEL);
		//item.setVisible(false);
		return super.onPrepareOptionsMenu(menu);			
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub		
		//if (item.getItemId()==Actividad.MENU_SALIR){		cerrarAplicacion=true;		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acercade);	
		mContext = getApplicationContext();
			
		//final TextView txtsitioweb = (TextView) findViewById(R.id.acercade_urlsitioweb);
		final TextView txtversion = (TextView) findViewById(R.id.acercade_codigoversion);
		
		

		
		//VERSION!
		try {		
			String versionNAME = getPackageManager().getPackageInfo(getPackageName(), 0).versionName  ;
			txtversion.setText(versionNAME);
		
		} catch (NameNotFoundException e) {
		    Log.e("tag", e.getMessage());
		}
		
		
		
		
		
	}
	
	private boolean MyStartActivity(Intent aIntent) {
	    try
	    {
	        startActivity(aIntent);
	        return true;
	    }
	    catch (ActivityNotFoundException e)
	    {           
	        return false;
	    }           
	}
	
}
