package amalgame.ventas;

import java.util.Date;

import amalgame.data.DatabaseManager;
import amalgame.entity.Actividad;

import amalgame.entity.Util;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCustomerAccountActivity extends Actividad{
	private Button btn_cancel;
	private Button btn_save;
	private EditText txt_costumerId;
	private EditText txt_name;
	private EditText txt_debit;
	private EditText txt_credit;
	private EditText txt_date;
	private Dialog dialog_confirmation;
	private Util utilman;
	private DatabaseManager manager;
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_customer_account);
		
		
		mContext = getApplicationContext();
		
		
		utilman = Util.getInstanceUtil();
		manager = DatabaseManager.getInstance();
		
		txt_costumerId = (EditText) findViewById(R.id.addcustomer_account__edittext_costumerId);
		txt_name = (EditText) findViewById(R.id.addcustomer_account_edittext_name);
		txt_debit = (EditText) findViewById(R.id.addcustomer_account_edittext_debit);
		txt_credit = (EditText) findViewById(R.id.addcustomer_account_edittext_credit);
		txt_date = (EditText) findViewById(R.id.addcustomer_account_edittext_Date);

		btn_cancel =  (Button)findViewById(R.id.addcustomer_account_button_cancel);
		btn_cancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	
		btn_save = (Button)findViewById(R.id.addcustomer_account_button_save);
		btn_save.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {			
				validate();				
			}
		});
		
	
	
		
		
	}
	
	
	

	
	private void validate(){
		
		if (txt_costumerId.getText().toString().equals("")
				|| txt_costumerId.getText().toString().equals("")) {
			dialog_confirmation = utilman
					.showDialog(
							getString(R.string.complete_all_required_fields_before_continue),
							getString(R.string.atention),
							AddCustomerAccountActivity.this
							);
			try {
				dialog_confirmation.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		else{
			
			
			/*
			 * 	public CustomerAccount(Integer costumerId, String name, float debit,
			float credit, Date date)
			 */
			
//			CustomerAccount c = new CustomerAccount(
//					Integer.parseInt(txt_costumerId.getText().toString().trim()), 
//					txt_name.getText().toString().trim(), 
//					Float.parseFloat(txt_debit.getText().toString().trim()), 
//					Float.parseFloat(txt_credit.getText().toString().trim()), 
//					new Date());
//			
//			manager.add(c);
			
			
			
			Toast.makeText(mContext, "The new customer account is added", Toast.LENGTH_LONG).show();
			
			finish();
		}
		
		
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
