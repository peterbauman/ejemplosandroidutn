package amalgame.ventas;

import java.util.ArrayList;
import java.util.List;

import amalgame.data.DataSQLiteHelper;
import amalgame.data.DatabaseManager;
import amalgame.entity.CustomAdapter;
import amalgame.entity.ListaActividad;
import amalgame.entity.db.Categorie;
import amalgame.entity.db.Product;
import amalgame.entity.db.Stock;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

public class StockActivity extends ListaActividad{
	  ListView listView;
	  List<Stock> stockList ;
	  List<Product> productList;
	  
	  List<String> stockListStr; 
	  List<String> productListStr; 
	  Dao<Stock, Integer> stockDao;
	  DataSQLiteHelper dataSQLiteHelper;
	  private ImageButton	  btn_ok;

	  private DatabaseManager manager;
	  private Context mContext;
	  private Dialog customDialog;
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stock);

		mContext = getApplicationContext();

		/*
		btn_ok = (ImageButton) findViewById(R.id.abm_categories_add);
		
		btn_ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
										
				customDialog = showDialog("Add a categorie", StockActivity.this);
				customDialog.show();
					
			}
		});
		*/		
		 manager = DatabaseManager.getInstance();
		
		 updateStockList_customAdapter();
		
		
		//##################################################################################################################
		 // delete
		 //##################################################################################################################		 
		 listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
					try {
						if(stockList.size()<=0) return;
						
						
						// ITEM
						int id  = stockList.get(position).getId();
						String name =  String.valueOf(stockList.get(position).getProduct_id());
						   		
						//TITTLE & TEXT
						String  title = getResources().getString(R.string.categoryproducts);
						//String text =  getResources().getString ( R.string.do_you_want_to_delete_this_item_) + " " + name;
				
						
						customDialog = showDialog(name, StockActivity.this);
						customDialog.show();
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		
	}
	
	
	/**
	 * @param String message
	 * @param String title
	 * @return 
	 */
	public Dialog showDialog( String title,final Context mContext){

			
			customDialog = new Dialog(mContext);
			customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);			
			customDialog.setContentView(R.layout.simpledialog_textedit);
			customDialog.setCancelable(false);		
			
			TextView tv_title =  (TextView) customDialog.findViewById(R.id.simpledialog_edittext_title);
			final TextView ed_text  = (TextView) customDialog.findViewById(R.id.simpledialog_edittext_desc);
			Button btn_ok     = (Button) customDialog.findViewById(R.id.simpledialog_edittext_ok);
			Button btn_cancel     = (Button) customDialog.findViewById(R.id.simpledialog_edittext_cancel);
		
			tv_title.setText(title);
						
			btn_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					String desc = ed_text.getText().toString().trim();
					
					if(!desc.equals("")){
						if (desc.length()>0 && desc.length()<20){
							
							Categorie c = new Categorie(0, desc);
							
							Categorie c_tmp = manager.getItemByName(c.getName());
							if(c_tmp==null){
								manager.addcategorie(c);
								updateStockList_customAdapter();
								customDialog.cancel();
							}else{
								Toast.makeText(mContext,getString( R.string.the_item_already_exists_ ),Toast.LENGTH_LONG).show();
							}													
						}
					}else{
						Toast.makeText(mContext,getString( R.string.you_must_insert_a_value_to_continue),Toast.LENGTH_LONG).show();
					}
			
				}
			});
			btn_cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					customDialog.cancel();
					//finish();
				}
			});
			
			return customDialog;
	}
	
	
	
	
	private void updateStockList_customAdapter(){
		stockList = manager.getAllStock();
		stockListStr = new ArrayList<String>();
		
		//Peter
		productList=manager.getAllproduct();
		stockListStr=new ArrayList<String>();
		
		listView = (ListView) getListView();
		
		if (stockList ==null){	
			stockListStr.add( getString(R.string.no_item_added)  );
			unregisterForContextMenu(listView);
		}else{
			for (Stock item : stockList) {
				String name=  "Name: "+manager.getProductByID(item.getProduct_id()).getName()+"\n";
				String quantity= "Quantity: "+String.valueOf(item.getQuantity())+"\n";
				String date= "Date: "+String.valueOf(item.getMovement_date());
				stockListStr.add(name+quantity+date);
				
			}
			registerForContextMenu(listView);
		}
		
		CustomAdapter customAdapter = new CustomAdapter(mContext, stockListStr,CustomAdapter.PRODUCT);

		listView.setAdapter(customAdapter);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateStockList_customAdapter();
	}
	
}
