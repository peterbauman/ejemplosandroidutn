package amalgame.ventas;

import amalgame.data.DatabaseManager;
import amalgame.entity.Actividad;
import amalgame.entity.Util;
import amalgame.entity.db.Customer;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AddCustomerActivity extends Actividad{
	private Button btn_cancel;
	private Button btn_save;
	private EditText txt_name;
	private EditText txt_cellphone;
	private EditText txt_email;
	private EditText txt_address;
	private EditText txt_city;
	private Dialog dialog_confirmation;
	private Util utilman;
	private DatabaseManager manager;
	private Context mContext;
	private boolean isEditMode = false;
	private Customer customer;
	private ImageButton imb_delete;
	private Dialog customDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_customer);
		
		mContext = getApplicationContext();
		
		
		utilman = Util.getInstanceUtil();
		manager = DatabaseManager.getInstance();
		
		 Bundle getuserID = getIntent().getExtras();
	        if (getuserID != null) {
		        String mode = getIntent().getExtras().getString("MODE");
		        if (mode.equals("EDIT")){
		        	isEditMode = true;
		        }
	        }
		
		txt_name = (EditText) findViewById(R.id.addcustomer_edittext_name);
		txt_cellphone = (EditText) findViewById(R.id.addcustomer_edittext_cellphone);
		txt_email = (EditText) findViewById(R.id.addcustomer_edittext_email);
		txt_address = (EditText) findViewById(R.id.addcustomer_edittext_address);
		txt_city = (EditText) findViewById(R.id.addcustomer_edittext_city);
		imb_delete = (ImageButton) findViewById(R.id.addcustomer_imageButton_delCustomer);
		
		imb_delete.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				try {
					int id = CustomerActivity.customerEdit.getCustomerID();
					//TITTLE & TEXT
					String  title = getResources().getString(R.string.customer);
					String text =  getResources().getString ( R.string.do_you_want_to_delete_this_item_) ;
					
					customDialog = showDialogEliminar(text,title,id	);
					customDialog.show();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		});
		
		btn_cancel =  (Button)findViewById(R.id.addcustomer_button_cancel);
		btn_cancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	
		btn_save = (Button)findViewById(R.id.addcustomer_button_save);
		btn_save.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {			
				validate();				
			}
		});
		
	
		if(isEditMode){
			btn_save.setText(getString(R.string.update));
	
			customer =  manager.getCustomerByID(CustomerActivity.customerEdit.getCustomerID());
			
			txt_name.setText(customer.getName().toString()); 
			txt_cellphone.setText(customer.getCel().toString());
			txt_email.setText(customer.getEmail().toString());
			txt_address.setText(customer.getAddress().toString());
			txt_city.setText( customer.getCity().toString());
			
		}
		
	}
	
	
	

	
	private void validate(){
		
		if (txt_name.getText().toString().equals("")
				|| txt_name.getText().toString().equals("")) {
			dialog_confirmation = utilman
					.showDialog(
							getString(R.string.complete_all_required_fields_before_continue),
							getString(R.string.atention),
							AddCustomerActivity.this
							);
			try {
				dialog_confirmation.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		else{
			
			
			if(customer!=null){
				customer.setName(txt_name.getText().toString());
				customer.setCel(txt_cellphone.getText().toString().trim());
				customer.setAddress(txt_address.getText().toString().trim());
				customer.setCity(txt_city.getText().toString().trim());
				customer.setEmail(txt_email.getText().toString().trim());
				
				
				
			}else{
				customer = new Customer(
						0, 
						txt_name.getText().toString().trim(), 
						txt_cellphone.getText().toString().trim(), 
						txt_email.getText().toString().trim(), 
						txt_address.getText().toString().trim(), 
						txt_city.getText().toString().trim());
			}
		
			if(isEditMode){
				
				
				boolean ret = manager.updateCustomer(customer);
				if(ret){
					Toast.makeText(mContext, getString(R.string.the_item_is_updated), Toast.LENGTH_LONG).show();	
				}else{
					Toast.makeText(mContext, getString(R.string.failed_to_update_the_item), Toast.LENGTH_LONG).show();
					
				}
				
			}else{
				manager.addCustomer(customer);
				
				Toast.makeText(mContext, "The new item is added", Toast.LENGTH_LONG).show();
			}
			
			
		
			
			finish();
		}
		
		
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	/**
	 * @param String message
	 * @param String title
	 * @return 
	 */
	public Dialog showDialogEliminar(String message, String title,final int ID){
			//
			
			customDialog = new Dialog(AddCustomerActivity.this);
			customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);			
			customDialog.setContentView(R.layout.simpledialog_okcancel);
			customDialog.setCancelable(false);		
			
			TextView tv_title =  (TextView) customDialog.findViewById(R.id.simpledialog_okcancel_title);
			TextView tv_text  = (TextView) customDialog.findViewById(R.id.simpledialog_okcancel_text);
			Button btn_ok     = (Button) customDialog.findViewById(R.id.simpledialog_okcancel_ok);
			Button btn_cancel     = (Button) customDialog.findViewById(R.id.simpledialog_okcancel_cancel);
		
			tv_title.setText(title);
			tv_text.setText(message);

		
			btn_ok.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					manager.deleteCustomerByID(ID);
					customDialog.cancel();
					finish();
				}
			});
			
			btn_cancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					customDialog.cancel();
					//finish();
				}
			});
	
			return customDialog;
	}
	
	
}
