package amalgame.ventas;

import amalgame.data.DatabaseManager;
import amalgame.entity.Actividad;
import amalgame.entity.Util;
import amalgame.entity.db.Customer;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditCustomerActivity extends Actividad{
	private Button btn_cancel;
	private Button btn_save;
	private EditText txt_name;
	private EditText txt_cellphone;
	private EditText txt_email;
	private EditText txt_address;
	private EditText txt_city;
	private Dialog dialog_confirmation;
	private Util utilman;
	private DatabaseManager manager;
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//***** RECIBE *******
		Intent intent = getIntent();
		
		String idReceived = intent.getStringExtra("idSalida");

	    String nameReceived = intent.getStringExtra("nameSalida");
	    String cellReceived = intent.getStringExtra("celSalida");
	    String emailReceived = intent.getStringExtra("emailSalida");
	    String addressReceived = intent.getStringExtra("addressSalida");
	    String cityReceived = intent.getStringExtra("citySalida");

		
		
		setContentView(R.layout.edit_customer);
		
		mContext = getApplicationContext();
		
		
		utilman = Util.getInstanceUtil();
		manager = DatabaseManager.getInstance();
		
		txt_name = (EditText) findViewById(R.id.editcustomer_edittext_name);
		txt_cellphone = (EditText) findViewById(R.id.editcustomer_edittext_cellphone);
		txt_email = (EditText) findViewById(R.id.editcustomer_edittext_email);
		txt_address = (EditText) findViewById(R.id.editcustomer_edittext_address);
		txt_city = (EditText) findViewById(R.id.editcustomer_edittext_city);

		txt_name.setText(nameReceived);
		txt_cellphone.setText(cellReceived);
		txt_email.setText(emailReceived);
		txt_address.setText(addressReceived);
		txt_city.setText(cityReceived);
		
		btn_cancel =  (Button)findViewById(R.id.editcustomer_button_cancel);
		btn_cancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	
		btn_save = (Button)findViewById(R.id.editcustomer_button_save);
		btn_save.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {			
				validate();				
			}
		});
		
	
	
		
		
	}
	
	
	

	
	private void validate(){
		
		if (txt_name.getText().toString().equals("")
				|| txt_name.getText().toString().equals("")) {
			dialog_confirmation = utilman
					.showDialog(
							getString(R.string.complete_all_required_fields_before_continue),
							getString(R.string.atention),
							EditCustomerActivity.this
							);
			try {
				dialog_confirmation.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		else{
			Customer c = new Customer(
					0, 
					txt_name.getText().toString().trim(), 
					txt_cellphone.getText().toString().trim(), 
					txt_email.getText().toString().trim(), 
					txt_address.getText().toString().trim(), 
					txt_city.getText().toString().trim());
			//manager.addCustomer(c);
			System.out.println(txt_name.getText().toString().trim());
			manager.updateCustomer(c);
			
			
			Toast.makeText(mContext, "The new customer is actualizated", Toast.LENGTH_LONG).show();
			
			finish();
		}
		
		
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
