package amalgame.ventas;

import java.util.ArrayList;
import java.util.List;

import amalgame.data.DataSQLiteHelper;
import amalgame.data.DatabaseManager;
import amalgame.entity.CustomAdapter;
import amalgame.entity.ListaActividad;
import amalgame.entity.Util;
import amalgame.entity.db.Customer;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

public class CustomerActivity extends ListaActividad {
	ListView listView;
	List<Customer> customerList;
	List<String> customerListStr;
	Dao<Customer, Integer> customerDao;
	DataSQLiteHelper dataSQLiteHelper;
	private ImageButton btn_add;
	private DatabaseManager manager;
	private Context mContext;
	private Util utilman;
	private Dialog customDialog;
	public static Customer customerEdit = null;

	
	private ArrayAdapter<String> adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.abm_customer);

		TextWatcher filterTextWatcher = new TextWatcher() {

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				adapter.getFilter().filter(s);
			}

			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}

		};

		mContext = getApplicationContext();
		utilman = Util.getInstanceUtil();

		btn_add = (ImageButton) findViewById(R.id.abm_customeradd);
		btn_add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent();
				intent.setClass(getApplicationContext(),
						AddCustomerActivity.class);
				startActivity(intent);

			}
		});

		manager = DatabaseManager.getInstance();

		updateCustomerList_customAdapter();

		// ##################################################################################################################
		// delete
		// ##################################################################################################################
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				try {
					if (customerList.size() <= 0)
						return;

					customerEdit = customerList.get(position);

					// ITEM
					int id = customerList.get(position).getCustomerID();
					String name = customerList.get(position).getName();

					// Dialog dialog = new Dialog(CustomerActivity.this);
					// dialog = showDialogEliminar(text, title, id);
					// dialog = showDialogModificar(text, title, position);
					// dialog.show();

					Intent intent = new Intent();
					intent.putExtra("MODE", "EDIT");
					intent.setClass(CustomerActivity.this,
							AddCustomerActivity.class);
					startActivity(intent);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});

	}

	private void updateCustomerList() {
		customerList = manager.getAllCustomer();
		customerListStr = new ArrayList<String>();
		for (Customer item : customerList) {
			customerListStr.add(item.getName());
		}
		listView = (ListView) getListView();

		ArrayAdapter<String> adapterCustomerList = new ArrayAdapter<String>(
				this, android.R.layout.simple_list_item_1, customerListStr);
		listView.setAdapter(adapterCustomerList);

	}

	private void updateCustomerList_customAdapter() {
		customerList = manager.getAllCustomer();
		customerListStr = new ArrayList<String>();
		listView = (ListView) getListView();
		if (customerList != null && customerList.size() > 0) {
			for (Customer item : customerList) {
				customerListStr.add(item.getName());
			}
			registerForContextMenu(listView);
		} else {
			customerListStr.add(getResources()
					.getString(R.string.no_item_added));
			unregisterForContextMenu(listView);
		}
		CustomAdapter customAdapter = new CustomAdapter(mContext,
				customerListStr);

		listView.setAdapter(customAdapter);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateCustomerList_customAdapter();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		updateCustomerList_customAdapter();
	}

	/**
	 * @param String
	 *            message
	 * @param String
	 *            title
	 * @return
	 */
	public Dialog showDialogEliminar(String message, String title, final int ID) {
		//

		customDialog = new Dialog(CustomerActivity.this);
		customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		customDialog.setContentView(R.layout.simpledialog_okcancel);
		customDialog.setCancelable(false);

		TextView tv_title = (TextView) customDialog
				.findViewById(R.id.simpledialog_okcancel_title);
		TextView tv_text = (TextView) customDialog
				.findViewById(R.id.simpledialog_okcancel_text);
		Button btn_ok = (Button) customDialog
				.findViewById(R.id.simpledialog_okcancel_ok);
		Button btn_cancel = (Button) customDialog
				.findViewById(R.id.simpledialog_okcancel_cancel);

		tv_title.setText(title);
		tv_text.setText(message);

		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				manager.deleteCustomerByID(ID);
				updateCustomerList_customAdapter();
				customDialog.cancel();
				// finish();
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.cancel();
				// finish();
			}
		});

		return customDialog;
	}

	/**
	 * @param String
	 *            message
	 * @param String
	 *            title
	 * @return
	 */
	public Dialog showDialogModificar(String message, String title, final int ID) {
		//

		customDialog = new Dialog(CustomerActivity.this);
		customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		customDialog.setContentView(R.layout.simpledialog_modify_delete_cancel);
		customDialog.setCancelable(true);

		TextView tv_title = (TextView) customDialog
				.findViewById(R.id.simpledialog_okcancel_title);
		TextView tv_text = (TextView) customDialog
				.findViewById(R.id.simpledialog_okcancel_text);
		Button btn_cancel = (Button) customDialog
				.findViewById(R.id.simpledialog_modify_delete_cancel_Cancel);
		Button btn_delete = (Button) customDialog
				.findViewById(R.id.simpledialog_modify_delete_cancel_Delete);
		Button btn_edit = (Button) customDialog
				.findViewById(R.id.simpledialog_modify_delete_cancel_EDIT);

		tv_title.setText(title);
		tv_text.setText(message);

		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				customDialog.cancel();
				// finish();
			}
		});

		btn_edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// ******DA******

				/*
				 * // ITEM int id = customerList.get(position).getId(); String
				 * name = customerList.get(position).getName();
				 * 
				 * //TITTLE & TEXT String title =
				 * getResources().getString(R.string.customer); String text =
				 * getResources().getString (
				 * R.string.do_you_want_to_delete_this_item_) + " " + name;
				 */

				Intent intent = new Intent();
				// intent.setClass(getApplicationContext(),
				// EditCustomerActivity.class);
				String id = String
						.valueOf(customerList.get(ID).getCustomerID());
				String name = customerList.get(ID).getName();
				String cel = customerList.get(ID).getCel();
				String email = customerList.get(ID).getEmail();
				String address = customerList.get(ID).getAddress();
				String city = customerList.get(ID).getCity();

				intent.putExtra("idSalida", id);
				intent.putExtra("nameSalida", name);
				intent.putExtra("celSalida", cel);
				intent.putExtra("emailSalida", email);
				intent.putExtra("addressSalida", address);
				intent.putExtra("citySalida", city);

				startActivity(intent);
				customDialog.cancel();
				// finish();
			}
		});

		return customDialog;
	}

	public void sendMessage(View view) {
		// Intent intent = new Intent(this, EditCustomerActivity.class);

		// startActivity(intent);
	}

}
